 <!DOCTYPE html>
<?php
session_start();
include("includes/header.php");
include("functions/functions.php");

if(!isset($_SESSION['user_email'])){
	header("location: index.php");

}
?>
<html>
<head>
	<title>Add your comment!</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="style/style.css">
	<link rel="stylesheet" href="style/home_style2.css">

</head>
<body>
	<div class="row">
	<div class="col-sm-12">
		<?php single_post();?>
	</div>
	</div>
</body>
</html>
