<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Project</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="style/style.css">
</head>

<body>
    <header>
        <div class="m-5">
            <h1 class="text-center text-light display-2">PHP Project</h1>
        </div>
    </header>

    <div class="pt-5">
        <h1 class="display-4 text-center text-light my-5">Login</h1>
        <div class="text-center my-5">
            <form action="login.php" method="post">
                <h4 class="text-light">Email</h4>
                <div class="form-group py-2">
                    <input type="email" name="email" placeholder="email" id="" class="login-form">
                </div>
                <h4 class="text-light">Password</h4>
                <div class="form-group py-2">
                    <input type="password" name="password" placeholder="password" id="" class="login-form">
                </div>
                <button type="submit" id="signin" name="login" class="btn btn-primary mt-2">Sign In</button>
            </form>
        </div>
    </div>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>

</html>